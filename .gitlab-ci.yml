stages:
  - test
  - publish
  - validate
  - plan
  - deploy
  - schedule

.meltano:
  image: python:3.8
  variables:
    MELTANO_ENVIRONMENT: cicd
    MELTANO_SEND_ANONYMOUS_USAGE_STATS: 'false'
    CI_BRANCH: b${CI_PIPELINE_IID}
  before_script:
  - cd data/
  - pip3 install meltano
  - echo "Installing creds file from CI..." && cp $MELTANO_ENV_FILE .env
  - export DEFAULT_START_DATE=$(date +%Y-%m-%d -d "1 day ago")
  rules:
    - if: $CI_PIPELINE_SOURCE != "schedule" && ($CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "master")
      when: on_success

.docker:
  variables:
    DOCKER_REGISTRY: 158444585956.dkr.ecr.us-east-1.amazonaws.com
    AWS_DEFAULT_REGION: us-east-1
    DOCKER_HOST: tcp://docker:2375
    AWS_PROFILE: production
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  services:
  - docker:dind
  before_script:
  - amazon-linux-extras install docker
  - aws --version
  - docker --version
  - aws ecr get-login-password | docker login --username AWS --password-stdin $DOCKER_REGISTRY
  rules:
  - if: $CI_COMMIT_BRANCH == "master"
    when: on_success

.terraform:
  image:
    name: hashicorp/terraform:light
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  variables:
    AWS_PROFILE: production
  before_script:
  - cd deploy/meltano
  - rm -rf .terraform
  - terraform --version
  - terraform init
  rules:
  - if: $CI_COMMIT_BRANCH == "master"
    when: on_success

# Airflow testing
airflow:
  extends:
  - .meltano
  stage: test
  script:
    - meltano install orchestrator airflow
    - meltano test airflow

# SQL linting
sqlfluff:
  extends:
  - .meltano
  stage: test
  script:
  - meltano install utilities sqlfluff
  - meltano install transformer dbt-snowflake
  - meltano invoke dbt-snowflake:deps
  - meltano invoke sqlfluff lint -v

# EL Testing
meltano_test_extracts:
  extends:
  - .meltano
  needs: [sqlfluff]
  parallel:
    matrix:
      - EXTRACT_SOURCE:
        - slack
        - google_analytics
        - gitlab
        - github_labs
        - github_search
  variables:
    # Override load schemas to include build prefix:
    TARGET_SNOWFLAKE_DEFAULT_TARGET_SCHEMA: "b${CI_PIPELINE_IID}_${EXTRACT_SOURCE}"
    DEFAULT_START_DATE: $DEFAULT_START_DATE
  script:
  - make install_extractor EXTRACT_SOURCE=${EXTRACT_SOURCE}
  - meltano install loader target-snowflake
  - make run_meltano EXTRACT_SOURCE=${EXTRACT_SOURCE}

# Transform testing
meltano_test_transform:
  extends:
  - .meltano
  stage: test
  needs: [meltano_test_extracts, sqlfluff]
  variables:
    CI_BRANCH: b${CI_PIPELINE_IID}
  script:
  - meltano install transformer dbt-snowflake
  - meltano invoke dbt-snowflake:deps
  - meltano invoke dbt-snowflake:compile
  - meltano invoke dbt-snowflake:seed
  - meltano invoke dbt-snowflake run
  - meltano invoke dbt-snowflake:test

# Publish testing
meltano_test_publish:
  extends:
  - .meltano
  stage: test
  needs: [meltano_test_transform]
  variables:
    CI_BRANCH: b${CI_PIPELINE_IID}
  script:
  - meltano install extractor tap-snowflake-metrics
  - meltano install extractor tap-snowflake-audit
  - meltano install loader target-yaml-metrics
  - meltano install loader target-yaml-audit
  - meltano run tap-snowflake-metrics target-yaml-metrics
  - meltano run tap-snowflake-audit target-yaml-audit

# Build & Publish Docker Images
docker-build-meltano:
  extends:
  - .docker
  stage: publish
  script:
  - docker pull $DOCKER_REGISTRY/m5o-prod-meltano:latest || true
  - >
    docker build
    --cache-from $DOCKER_REGISTRY/m5o-prod-meltano:latest
    -t $DOCKER_REGISTRY/m5o-prod-meltano:latest
    -t $DOCKER_REGISTRY/m5o-prod-meltano:$CI_COMMIT_SHA
    -f deploy/meltano/Dockerfile.meltano
    data/
  - docker push $DOCKER_REGISTRY/m5o-prod-meltano:$CI_COMMIT_SHA
  - docker push $DOCKER_REGISTRY/m5o-prod-meltano:latest
  needs: ["meltano_test_publish"]

# Build and Publish Airflow Image
docker-build-airflow:
  extends:
  - .docker
  stage: publish
  script:
  - docker pull $DOCKER_REGISTRY/m5o-prod-airflow:latest || true
  - >
    docker build
    --cache-from $DOCKER_REGISTRY/m5o-prod-airflow:latest
    -t $DOCKER_REGISTRY/m5o-prod-airflow:latest
    -t $DOCKER_REGISTRY/m5o-prod-airflow:$CI_COMMIT_SHA
    -f deploy/meltano/Dockerfile.airflow
    data/
  - docker push $DOCKER_REGISTRY/m5o-prod-airflow:$CI_COMMIT_SHA
  - docker push $DOCKER_REGISTRY/m5o-prod-airflow:latest
  needs: ["meltano_test_publish"]

# Deploy New Images
terraform-validate:
  extends:
  - .terraform
  stage: validate
  script:
    - terraform validate
  needs: ["docker-build-airflow", "docker-build-meltano"]

terraform-plan:
  extends:
  - .terraform
  stage: plan
  script:
    - export TF_VAR_airflow_image_tag=$CI_COMMIT_SHA
    - export TF_VAR_meltano_image_tag=$CI_COMMIT_SHA
    - terraform plan -out "planfile"
  dependencies:
    - terraform-validate
  artifacts:
    paths:
      - deploy/meltano/planfile
  needs: ["terraform-validate"]

terraform-apply:
  extends:
  - .terraform
  stage: deploy
  script:
    - terraform apply -input=false "planfile"
  dependencies:
    - terraform-plan
  needs: ["terraform-plan"]

# Deploy dbt Seed file changes
dbt_seed:
  extends:
  - .meltano
  stage: deploy
  variables:
    SNOWFLAKE_PASSWORD: $SNOWFLAKE_PASSWORD_PROD
  script:
  - meltano install transformer dbt-snowflake
  - meltano invoke dbt-snowflake:deps
  - meltano --environment=prod invoke dbt-snowflake:seed
  rules:
  - if: $CI_COMMIT_BRANCH == "master"
    when: on_success
  needs: ["terraform-apply"]

pages:
  extends:
  - .meltano
  stage: deploy
  variables:
    SNOWFLAKE_PASSWORD: $SNOWFLAKE_PASSWORD_PROD
  script:
  - meltano install transformer dbt-snowflake
  - meltano invoke dbt-snowflake:deps
  - meltano --environment=prod invoke dbt-snowflake docs generate
  - cd .meltano/transformers/dbt/target
  - | # remove row counts
    sed -i 's/"Row Count", "value": [0-9]*.0/"Row Count", "value": -1.0/g' catalog.json
  - mkdir -p ${CI_PROJECT_DIR}/public/
  - mv * ${CI_PROJECT_DIR}/public
  artifacts:
    paths:
      - public
  rules:
  - if: $CI_COMMIT_BRANCH == "master"
    when: on_success
  needs: ["terraform-apply"]

clean_snowflake_ci_schemas:
  extends:
  - .meltano
  stage: schedule
  script:
  - meltano install transformer dbt-snowflake
  - meltano invoke dbt-snowflake:deps
  - meltano invoke dbt-snowflake run-operation clear_ci_schemas
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: always
