plugins:
  extractors:
  - name: tap-slack
    namespace: tap_slack
    pip_url: git+https://github.com/MeltanoLabs/tap-slack.git@f0205cd4016d6cd4f46cf11841aaffe13fb2cc8d
    executable: tap-slack
    capabilities:
    - catalog
    - state
    - discover
    - about
    - stream-maps
    settings:
    - name: api_key
      kind: password
    config:
      start_date: '2021-01-01'
      auto_join_channels: false
      selected_channels:
      - C01SK13R9NJ
      channel_types:
      - private_channel
    select:
    - users.*
    - channels.*
    - messages.*
  - name: tap-google-analytics
    namespace: tap_google_analytics
    pip_url: git+https://github.com/MeltanoLabs/tap-google-analytics.git
    executable: tap-google-analytics
    capabilities:
    - catalog
    - discover
    - state
    settings:
    - name: start_date
      kind: date_iso8601
    - name: client_secrets
      kind: object
    config:
      view_id: '188392047' # CLI
      # view_id: '188274549' # Meltano Website
      # view_id: '188384771' # Meltano UI
      # view_id: '210710793' # Newsletter
      reports: ./extract/ga_reports/cli_events_report_definition.json
  - name: tap-gitlab
    variant: meltanolabs
    pip_url: git+https://github.com/pnadolny13/tap-gitlab.git@author_usernames
    select:
    - projects.*
    - merge_requests.*
    - issues.*
  - name: tap-github
    namespace: tap_github
    pip_url: git+https://github.com/MeltanoLabs/tap-github@d99378778c0cebc446c12b552ee4fd386fdc2610
    capabilities:
    - catalog
    - discover
    - state
    settings:
    - name: auth_token
      kind: password
  - name: tap-github-labs
    inherit_from: tap-github
    config:
      organizations:
      - MeltanoLabs
      stream_maps:
        issues:
          __filter__: record['type'] = 'issue'
    select:
    - repositories.*
    - pull_requests.*
    - issues.*
  - name: tap-github-search
    inherit_from: tap-github
    config:
      start_date: '2022-02-13'
      searches:
      - name: tap forks
        query: tap- fork:only language:Python singer in:readme
      - name: tap non-forks
        query: tap- fork:false language:Python singer in:readme
      - name: targets forks
        query: target- fork:only language:Python singer in:readme
      - name: target non-forks
        query: target- fork:false language:Python singer in:readme
      metrics_log_level: DEBUG
      stream_maps:
        repositories:
          __filter__: >
            ('tap-' in name or 'target-' in full_name)
            and name != 'singer-tap-template'
            and name != 'singer-target-template'
    select:
    - repositories.*
    # - issues.*
    # - issue_comments.*
    - '!*.comments'
    - '!*.body'
    - '!*.reactions'  # Broken JSON Schema validator for issues
  - name: tap-snowflake
    namespace: tap_snowflake
    pip_url: git+https://github.com/transferwise/pipelinewise-tap-snowflake.git@15699411cc0ff87f11a3f7e637f01a2eb8cbf9da
    executable: tap-snowflake
    capabilities:
    - properties
    - discover
    - state
    settings:
    - name: account
    - name: dbname
    - name: user
    - name: password
      kind: password
    - name: warehouse
    - name: tables
    - name: role
    config:
      account: epa06486
      password: ${SNOWFLAKE_PASSWORD}
  - name: tap-snowflake-metrics
    inherit_from: tap-snowflake
    metadata:
      '*':
        replication-method: FULL_TABLE
    select:
      - '*FACT_HUB_METRICS.REPO_FULL_NAME'
      - '*FACT_HUB_METRICS.CREATED_AT_TIMESTAMP'
      - '*FACT_HUB_METRICS.LAST_PUSH_TIMESTAMP'
      - '*FACT_HUB_METRICS.LAST_UPDATED_TIMESTAMP'
      - '*FACT_HUB_METRICS.NUM_FORKS'
      - '*FACT_HUB_METRICS.NUM_OPEN_ISSUES'
      - '*FACT_HUB_METRICS.NUM_STARGAZERS'
      - '*FACT_HUB_METRICS.NUM_WATCHERS'
      - '*FACT_HUB_METRICS.MELTANO_EXEC_COUNT_3M'
      - '*FACT_HUB_METRICS.MELTANO_PROJECT_ID_COUNT_3M'
  - name: tap-snowflake-audit
    inherit_from: tap-snowflake
    metadata:
      '*':
        replication-method: FULL_TABLE
    select:
    - '*HUB_METRICS_AUDIT.UPDATED_DATE'
    - '*HUB_METRICS_AUDIT.METRIC_TYPE'